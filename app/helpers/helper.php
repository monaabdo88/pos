<?php
if(! function_exists('active_link')){
    function active_link($path){
        $path = explode('.', $path);
        $segment = 3;
        foreach($path as $p) {
            if((request()->segment($segment) == $p) == false) {
                return '';
            }
                $segment++;
            }
        return 'active';
    }
    
}