<?php
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){ 
        Route::prefix('cp')->name('cp.')->middleware(['auth'])->group(function () {
            Route::get('/','DashboardController@index')->name('index');
            //category routes
            Route::resource('categories', 'CategoryController')->except(['show']);
            Route::get('categories/delCat/{id}','CategoryController@destroy');
            Route::get('categoriesAll/delet_all_categories/{ids}','CategoryController@del_all');
            //product routes
            Route::resource('products', 'ProductController')->except(['show']);
            Route::get('products/delProduct/{id}','ProductController@destroy');
            Route::get('catProducts/{id}','ProductController@catProducts');
            Route::get('productsAll/delet_all_products/{ids}','ProductController@del_all');
            //client routes
            Route::resource('clients', 'ClientController')->except(['show']);
            Route::resource('clients.orders', 'ClientOrdersController')->except(['show']);
            Route::get('clients/delClient/{id}','ClientController@destroy');
            Route::get('clientsAll/delet_all_clients/{ids}','ClientController@del_all');
            //order routes
            Route::resource('orders', 'OrderController');
            Route::get('/orders/{order}/products', 'OrderController@products')->name('orders.products');
            Route::get('orders/delOrder/{id}','OrderController@destroy');
            
            //user routes
            Route::resource('users', 'UserController')->except(['show']);
            Route::get('users/delUser/{id}','UserController@destroy');
            Route::get('usersAll/delet_all_users/{ids}','UserController@del_all');
    
        });
    });