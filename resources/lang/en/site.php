<?php

return [
    'dashboard' => 'Home',
    'logout' => 'Logout',
    'cpanel'    =>'Cpanel',
    'add' => 'Add',
    'create' => 'Create',
    'read' => 'Read',
    'edit' => 'Edit',
    'update' => 'Update',
    'delete' => 'Delete',
    'search' => 'Search',
    'show' => 'Show',
    'loading' => 'Loading',
    'print' => 'Print',
    'add_new_user'  =>'Add New User',
    
    'confirm_delete' => 'Confirm Delete',
    'delete_msg'    => 'This record and it`s details will be permanantly deleted!',
    'yes' => 'Yes',
    'no' => 'No',
    'del_all'=>'Delete Selected',
    'login' => 'Login',
    'remember_me' => 'Remember me',
    'password' => 'Password',
    'password_confirmation' => 'Confirm Password',

    'added_successfully' => 'Data Added Successfully',
    'updated_successfully' => 'Updated data Successfully',
    'deleted_successfully' => 'Deleted Data Successfully',

    'no_data_found' => 'Sorry no data found',
    'no_records' => 'No Records Found',

    'clients' => 'Clients',
    'client_name' => 'Client Name',
    'phone' => 'Phone',
    'address' => 'Address',
    'previous_orders' => 'Previous Orders',
    'orders' => 'Orders',
    'add_order' => 'Add Order',
    'edit_order' => 'Edit Order',

    'users' => 'Users',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'E-mail',
    'image' => 'Image',
    'action' => 'Action',

    'permissions' => 'Permissions',

    'categories' => 'Categories',
    'all_categories' => 'All Categories',
    'name' => 'Name',
    'description' => 'Description',
    'products_count' => 'Products Count',
    'related_products' => 'Related Products',
    'category' => 'Category',
    'show_products' => 'Show Products',
    'created_at' => 'Created At',

    'products' => 'Products',
    'product' => 'Product',
    'quantity' => 'Quantity',
    'total' => 'Total',
    'purchase_price' => 'Purchase Price',
    'price' => 'Price',
    'sale_price' => 'Sale Price',
    'stock' => 'Stock',
    'profit_percent' => 'Profit Percent',
    
    'ar' => [
        'name' => 'Arabic Name',
        'description' => 'Arabic Description',
    ],

    'en' => [
        'name' => 'English Name',
        'description' => 'English Description',
    ],

];
