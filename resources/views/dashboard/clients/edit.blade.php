<div class="modal fade" id="editModal_{{$client->id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">@lang('site.edit') </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        @include('partials._errors')
        <div class="modal-body">
            <form action="{{ route('cp.clients.update', $client->id) }}" method="post" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                @csrf
                <div class="form-group">
                    <label>@lang('site.name')</label>
                    <input type="text" name="name" class="form-control" value="{{ $client->name }}">
                </div>

                @for ($i = 0; $i < 2; $i++)
                    <div class="form-group">
                        <label>@lang('site.phone')</label>
                        <input type="text" name="phone[]" class="form-control" value="{{ $client->phone[$i] ?? '' }}">
                    </div>
                @endfor

                <div class="form-group">
                    <label>@lang('site.address')</label>
                    <textarea name="address" class="form-control">{{ $client->address }}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> @lang('site.edit')</button>
                </div>

            </form><!-- end of form -->
        </div>
    </div>
    </div>
</div>