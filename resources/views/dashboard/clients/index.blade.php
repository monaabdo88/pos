@extends('dashboard.layouts.app')
@section('content')
<section class="content-header">

        <h1>@lang('site.clients')</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('cp.index') }}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active">@lang('site.clients')</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                
              <h3 class="box-title">@lang('site.clients') : {{$clients->count()}}</h3>
              
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                @include('notify::messages')
                @if(auth()->user()->hasPermission('create_clients'))
                    <button type="button" class="btn btn-success float-left" data-toggle="modal" data-target="#addModal">
                        <i class="fa fa-plus"></i> @lang('site.add')
                    </button>
                @endif
                  <br><br>
                  <!-- Add Modal -->
                @include('dashboard.clients.create')
                @if($errors->any())
                    <div class="alert alert-danger"><p>{{$errors->first()}}</p></div>
                @endif   
                  <table class="table table-bordered data-table display select" id="clients_tbl">
                    <thead>
                        <tr>
                            <th class="no-sort"><input type="checkbox" class="selectAll" value="0"/></th>                                
                            <th>#</th>
                            <th>@lang('site.name')</th>
                                <th>@lang('site.phone')</th>
                                <th>@lang('site.address')</th>
                                <th>@lang('site.add_order')</th>
                                <th>@lang('site.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($clients as $key=>$client)
                        <tr>
                            <td><input type="checkbox" name="all_Items[]" value="{{$client->id}}" class="checkItem"></td>
                            <td>{{$key+1}}</td>
                            <td>{{ $client->name }}</td>
                            <td>{{ is_array($client->phone) ? implode($client->phone, '-') : $client->phone }}</td>
                            <td>{{ $client->address }}</td>
                            <td>
                                @if (auth()->user()->hasPermission('create_orders'))
                                    <a href="{{ route('cp.clients.orders.create', $client->id) }}" class="btn btn-primary btn-sm">@lang('site.add_order')</a>
                                @else
                                    <a href="#" class="btn btn-primary btn-sm disabled">@lang('site.add_order')</a>
                                @endif
                            </td>
                            <td>  
                                @if(auth()->user()->hasPermission('update_clients'))  
                                    <button class="btn btn-warning" data-toggle="modal" data-target="#editModal_{{$client->id}}"><i class="fa fa-pencil"></i></button>
                                @endif
                                @if(auth()->user()->hasPermission('delete_clients'))
                                    <a href="{{url('cp/clients/delClient/'.$client->id)}}" class="btn btn-danger delete-confirm"><i class="fa fa-trash"></i></a>
                                @endif
                                
                            </td>
                        </tr>
                            @if(auth()->user()->hasPermission('update_clients'))
                                @include('dashboard.clients.edit')
                            @endif
                        @endforeach

                    </tbody>
                 
                </table>
                @if(auth()->user()->hasPermission('delete_clients'))
                    <button class="btn btn-danger float-right confirm_all" type="submit" disabled>@lang('site.del_all')</button>
                @endif
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
  
           
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
</section>
@push('scripts')
    
    <script>
        $(document).ready(function() {
            $('#clients_tbl').DataTable({
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false,
                    "order": []
                } ]
            } );
        });
        
       // confirm delete
        $('.delete-confirm').on('click', function (event) {
            event.preventDefault();
            const url = $(this).attr('href');
            swal({
                title: '@lang('site.confirm_delete')',
                text: '@lang('site.delete_msg')',
                icon: 'warning',
                buttons: ["@lang('site.no')", "@lang('site.yes')"],
            }).then(function(value) {
                if (value) {
                    window.location.href = url;
                }
            });
        });
        // enable check all items
        $('.selectAll').click(function () {    
            $(':checkbox.checkItem').prop('checked', this.checked);    
        }); 
        //show delete all button after check checkboxs
        $('.check:button').click(function () {
            var checked = !$(this).data('checked');
            $('input:checkbox').prop('checked', checked);
            $('.confirm_all').prop('disabled', !checked)
            $(this).data('checked', checked);
            if (checked == true) {
                $(this).val('Uncheck All');
            } else if (checked == false) {
                $(this).val('Check All');
            }
        });
        $('input:checkbox').change(function () {
            $('.confirm_all').prop('disabled', $('input:checkbox:checked').length == 0)
        })
        // confirm before delete All
        $('.confirm_all').click(function () {
            event.preventDefault();
            var ids = [];
            swal({
                title: '@lang('site.confirm_delete')',
                text: '@lang('site.delete_msg')',
                icon: 'warning',
                buttons: ["@lang('site.no')", "@lang('site.yes')"],
            }).then(function(value) {
                if (value) {
                    $.each($('.checkItem:checked'),function(){ 
                        if($(this).val() != 0)
                        ids.push($(this).val()); 
                    });
                    window.location.href = "{{url('cp/clientsAll/delet_all_clients')}}"+'/'+ids;
                }
            });
        })
        
    </script>
@endpush   
@endsection