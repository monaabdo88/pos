<header class="main-header">
    <!-- Logo -->
    <a href="{{url('/cp')}}" class="logo">
      {{config('app.name')}} | @lang('site.cpanel')
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              
            </a>
            <ul class="dropdown-menu">
              <li>
                {{--<!-- inner menu: contains the actual data -->--}}
                <ul class="menu">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <li>
                            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                {{ $properties['native'] }}
                            </a>
                        </li>
                    @endforeach
                </ul>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{asset('uploads/users/'.auth()->user()->image)}}" class="user-image" alt="User Image">
              <span class="hidden-xs"></span>
              {{auth()->user()->first_name}} {{auth()->user()->last_name}}
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{asset('uploads/users/'.auth()->user()->image)}}" class="img-circle" alt="User Image">

                <p>
                  {{auth()->user()->first_name}} {{auth()->user()->last_name}}
                  <small>{{auth()->user()->created_at->diffForHumans()}} </small>
                </p>
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">


                <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">@lang('site.logout')</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </li>
            </ul>
          </li>
         
        </ul>
      </div>

    </nav>
  </header>