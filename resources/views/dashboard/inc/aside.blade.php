<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
        <img src="{{asset('uploads/users/'.auth()->user()->image)}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
        <p>{{auth()->user()->first_name}} {{auth()->user()->last_name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="sidebar-item {{active_link('')}}"><a href="{{ route('cp.index') }}"><i class="fa fa-home"></i><span>@lang('site.dashboard')</span></a></li>
        @if (auth()->user()->hasPermission('read_categories'))
          <li class="sidebar-item {{active_link('categories')}}"><a href="{{ route('cp.categories.index') }}"><i class="fa fa-th"></i><span>@lang('site.categories')</span></a></li>
        @endif
        @if (auth()->user()->hasPermission('read_products'))
          <li class="sidebar-item {{active_link('products')}}"><a href="{{ route('cp.products.index') }}"><i class="fa  fa-shopping-cart"></i><span>@lang('site.products')</span></a></li>
        @endif
        @if (auth()->user()->hasPermission('read_clients'))
          <li class="sidebar-item {{active_link('clients')}}"><a href="{{ route('cp.clients.index') }}"><i class="fa  fa-users"></i><span>@lang('site.clients')</span></a></li>
        @endif
        @if (auth()->user()->hasPermission('read_orders'))
          <li class="sidebar-item {{active_link('orders')}}"><a href="{{ route('cp.orders.index') }}"><i class="fa fa-money"></i><span>@lang('site.orders')</span></a></li>
        @endif
        @if (auth()->user()->hasPermission('read_users'))
          <li class="sidebar-item {{active_link('users')}}"><a href="{{ route('cp.users.index') }}"><i class="fa fa-user-secret"></i><span>@lang('site.users')</span></a></li>
        @endif
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>