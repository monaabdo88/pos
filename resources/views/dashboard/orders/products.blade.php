@extends('dashboard.layouts.app')
@section('content')
<section class="content-header">

        <h1>@lang('site.products')</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('cp.index') }}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active">@lang('site.prdocuts')</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3>@lang('site.total') <span>{{ number_format($order->total_price, 2) }}</span></h3>

              
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                
                @if($errors->any())
                    <div class="alert alert-danger"><p>{{$errors->first()}}</p></div>
                @endif   
                  <table class="table table-bordered data-table display select" id="orders_tbl">
                    <thead>
                        <tr>
                            <th>@lang('site.name')</th>
                            <th>@lang('site.quantity')</th>
                            <th>@lang('site.price')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->pivot->quantity }}</td>
                            <td>{{ number_format($product->pivot->quantity * $product->sale_price, 2) }}</td>
                        </tr>
                        @endforeach

                    </tbody>
                 
                </table>
                
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
  
           
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
</section>
@push('scripts')
    
    <script>
        $(document).ready(function() {
            $('#orders_tbl').DataTable({
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false,
                    "order": []
                } ]
            } );
        });       
    </script>
@endpush   
@endsection