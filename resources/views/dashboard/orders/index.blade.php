@extends('dashboard.layouts.app')
@section('content')
<section class="content-header">

        <h1>@lang('site.orders')</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('cp.index') }}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active">@lang('site.orders')</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                
              <h3 class="box-title">@lang('site.orders') : {{$orders->count()}}</h3>
              
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                @include('notify::messages')
                
                  <!-- Add Modal -->
                @if($errors->any())
                    <div class="alert alert-danger"><p>{{$errors->first()}}</p></div>
                @endif   
                  <table class="table table-bordered data-table display select" id="orders_tbl">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('site.client_name')</th>
                            <th>@lang('site.price')</th>
                            <th>@lang('site.created_at')</th>
                            <th>@lang('site.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $key=>$order)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{ $order->client->name }}</td>
                            <td>{{ number_format($order->total_price, 2) }}</td>
                            <td>{{ $order->created_at->toFormattedDateString() }}</td>
                            <td>  
                                @if(auth()->user()->hasPermission('update_orders'))  
                                    <a href="{{ route('cp.clients.orders.edit', ['client' => $order->client->id, 'order' => $order->id]) }}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if(auth()->user()->hasPermission('delete_orders'))
                                    <a href="{{url('cp/orders/delOrder/'.$order->id)}}" class="btn btn-danger delete-confirm"><i class="fa fa-trash"></i></a>
                                @endif
                                
                            </td>
                        </tr>
                            
                        @endforeach

                    </tbody>
                 
                </table>
               
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
  
           
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
</section>
@push('scripts')
    
    <script>
        $(document).ready(function() {
            $('#orders_tbl').DataTable({
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false,
                    "order": []
                } ]
            } );
        });
        
       // confirm delete
        $('.delete-confirm').on('click', function (event) {
            event.preventDefault();
            const url = $(this).attr('href');
            swal({
                title: '@lang('site.confirm_delete')',
                text: '@lang('site.delete_msg')',
                icon: 'warning',
                buttons: ["@lang('site.no')", "@lang('site.yes')"],
            }).then(function(value) {
                if (value) {
                    window.location.href = url;
                }
            });
        });
        
        
        
    </script>
@endpush   
@endsection