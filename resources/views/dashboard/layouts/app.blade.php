<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{config('app.name')}} | @lang('site.cpanel')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 {{--<!-- Bootstrap 3.3.7 -->--}}
 <link rel="stylesheet" href="{{ asset('dashboard_files/css/bootstrap.min.css') }}">
 <link rel="stylesheet" href="{{ asset('dashboard_files/css/ionicons.min.css') }}">
 <link rel="stylesheet" href="{{ asset('dashboard_files/css/skin-blue.min.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
@notifyCss
 
 @if (app()->getLocale() == 'ar')
     <link rel="stylesheet" href="{{ asset('dashboard_files/css/font-awesome-rtl.min.css') }}">
     <link rel="stylesheet" href="{{ asset('dashboard_files/css/AdminLTE-rtl.min.css') }}">
     <link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">
     <link rel="stylesheet" href="{{ asset('dashboard_files/css/bootstrap-rtl.min.css') }}">
     <link rel="stylesheet" href="{{ asset('dashboard_files/css/rtl.css') }}">

     <style>
         body, h1, h2, h3, h4, h5, h6 {
             font-family: 'Cairo', sans-serif !important;
         }
         .connectify-alert{
             position: fixed;
             top: 40px;
             right: 230px;
             direction: ltr
         }
     </style>
 @else
     <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
     <link rel="stylesheet" href="{{ asset('dashboard_files/css/font-awesome.min.css') }}">
     <link rel="stylesheet" href="{{ asset('dashboard_files/css/AdminLTE.min.css') }}">
 @endif
 <style>
     .mr-2{
         margin-right: 5px;
     }

     .loader {
         border: 5px solid #f3f3f3;
         border-radius: 50%;
         border-top: 5px solid #367FA9;
         width: 60px;
         height: 60px;
         -webkit-animation: spin 1s linear infinite; /* Safari */
         animation: spin 1s linear infinite;
     }

     /* Safari */
     @-webkit-keyframes spin {
         0% {
             -webkit-transform: rotate(0deg);
         }
         100% {
             -webkit-transform: rotate(360deg);
         }
     }

     @keyframes spin {
         0% {
             transform: rotate(0deg);
         }
         100% {
             transform: rotate(360deg);
         }
     }

 </style>
 {{--<!-- jQuery 3 -->--}}
 <script src="{{ asset('dashboard_files/js/jquery.min.js') }}"></script>

 

 {{--morris--}}
 <link rel="stylesheet" href="{{ asset('dashboard_files/plugins/morris/morris.css') }}">

 {{--<!-- iCheck -->--}}
 <link rel="stylesheet" href="{{ asset('dashboard_files/plugins/icheck/all.css') }}">

 {{--html in  ie--}}
 <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
 <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('dashboard.inc.header')
  <!-- Left side column. contains the logo and sidebar -->
  @include('dashboard.inc.aside')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
    
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
<strong>Copyright &copy; 2014-2019 <a href="https://mona-abdo.com">{{config('app.name')}}</a>.</strong> All rights
    reserved.
  </footer>

  
</div>
<!-- ./wrapper -->

{{--<!-- Bootstrap 3.3.7 -->--}}
<script src="{{ asset('dashboard_files/js/bootstrap.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

{{--icheck--}}
<script src="{{ asset('dashboard_files/plugins/icheck/icheck.min.js') }}"></script>

{{--<!-- FastClick -->--}}
<script src="{{ asset('dashboard_files/js/fastclick.js') }}"></script>

{{--<!-- AdminLTE App -->--}}
<script src="{{ asset('dashboard_files/js/adminlte.min.js') }}"></script>

{{--ckeditor standard--}}
<script src="{{ asset('dashboard_files/plugins/ckeditor/ckeditor.js') }}"></script>

{{--jquery number--}}
<script src="{{ asset('dashboard_files/js/jquery.number.min.js') }}"></script>

{{--print this--}}
<script src="{{ asset('dashboard_files/js/printThis.js') }}"></script>

{{--morris --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('dashboard_files/plugins/morris/morris.min.js') }}"></script>

{{--custom js--}}
<script src="{{ asset('dashboard_files/js/custom/image_preview.js') }}"></script>
<script src="{{ asset('dashboard_files/js/custom/order.js') }}"></script>
@stack('scripts')
<script>
    $(document).ready(function () {

        //icheck
        /*$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });*/
        
        CKEDITOR.config.language =  "{{ app()->getLocale() }}";

    });//end of ready
    
</script>
@notifyJs
</body>
</html>
