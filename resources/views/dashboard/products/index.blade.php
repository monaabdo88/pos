@extends('dashboard.layouts.app')
@section('content')
<section class="content-header">

        <h1>@lang('site.products')</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('cp.index') }}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active">@lang('site.products')</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                
              <h3 class="box-title">@lang('site.products') : {{$products->count()}}</h3>
              
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                @include('notify::messages')
                @if(auth()->user()->hasPermission('create_products'))
                    <button type="button" class="btn btn-success float-left" data-toggle="modal" data-target="#addModal">
                        <i class="fa fa-plus"></i> @lang('site.add')
                    </button>
                @endif
                  <br><br>
                  <!-- Add Modal -->
                @include('dashboard.products.create')
                @if($errors->any())
                    <div class="alert alert-danger"><p>{{$errors->first()}}</p></div>
                @endif   
                  <table class="table table-bordered data-table display select" id="products_tbl">
                    <thead>
                        <tr>
                            <th class="no-sort"><input type="checkbox" class="selectAll" value="0"/></th>                                
                            <th>#</th>
                            <th>@lang('site.name')</th>
                            <th>@lang('site.category')</th>
                            <th>@lang('site.image')</th>
                            <th>@lang('site.purchase_price')</th>
                            <th>@lang('site.sale_price')</th>
                            <th>@lang('site.profit_percent') %</th>
                            <th>@lang('site.stock')</th>
                            <th>@lang('site.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $key=>$product)
                        <tr>
                            <td><input type="checkbox" name="all_Items[]" value="{{$product->id}}" class="checkItem"></td>
                            <td>{{$key+1}}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->category->name }}</td>
                            <td><img src="{{ $product->image_path }}" style="width: 100px"  class="img-thumbnail" alt=""></td>
                            <td>{{ $product->purchase_price }}</td>
                            <td>{{ $product->sale_price }}</td>
                            <td>{{ $product->profit_percent }} %</td>
                            <td>{{ $product->stock }}</td>
                            <td>  
                                @if(auth()->user()->hasPermission('update_products'))  
                                    <button class="btn btn-warning" data-toggle="modal" data-target="#editModal_{{$product->id}}"><i class="fa fa-pencil"></i></button>
                                @endif
                                @if(auth()->user()->hasPermission('delete_products'))
                                    <a href="{{url('cp/products/delProduct/'.$product->id)}}" class="btn btn-danger delete-confirm"><i class="fa fa-trash"></i></a>
                                @endif
                                
                            </td>
                        </tr>
                            @if(auth()->user()->hasPermission('update_products'))
                                @include('dashboard.products.edit')
                            @endif
                        @endforeach

                    </tbody>
                 
                </table>
                @if(auth()->user()->hasPermission('delete_products'))
                    <button class="btn btn-danger float-right confirm_all" type="submit" disabled>@lang('site.del_all')</button>
                @endif
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
  
           
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
</section>
@push('scripts')
    
    <script>
        $(document).ready(function() {
            $('#products_tbl').DataTable({
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false,
                    "order": []
                } ]
            } );
        });
        
       // confirm delete
        $('.delete-confirm').on('click', function (event) {
            event.preventDefault();
            const url = $(this).attr('href');
            swal({
                title: '@lang('site.confirm_delete')',
                text: '@lang('site.delete_msg')',
                icon: 'warning',
                buttons: ["@lang('site.no')", "@lang('site.yes')"],
            }).then(function(value) {
                if (value) {
                    window.location.href = url;
                }
            });
        });
        // enable check all items
        $('.selectAll').click(function () {    
            $(':checkbox.checkItem').prop('checked', this.checked);    
        }); 
        //show delete all button after check checkboxs
        $('.check:button').click(function () {
            var checked = !$(this).data('checked');
            $('input:checkbox').prop('checked', checked);
            $('.confirm_all').prop('disabled', !checked)
            $(this).data('checked', checked);
            if (checked == true) {
                $(this).val('Uncheck All');
            } else if (checked == false) {
                $(this).val('Check All');
            }
        });
        $('input:checkbox').change(function () {
            $('.confirm_all').prop('disabled', $('input:checkbox:checked').length == 0)
        })
        // confirm before delete All
        $('.confirm_all').click(function () {
            event.preventDefault();
            var ids = [];
            swal({
                title: '@lang('site.confirm_delete')',
                text: '@lang('site.delete_msg')',
                icon: 'warning',
                buttons: ["@lang('site.no')", "@lang('site.yes')"],
            }).then(function(value) {
                if (value) {
                    $.each($('.checkItem:checked'),function(){ 
                        if($(this).val() != 0)
                        ids.push($(this).val()); 
                    });
                    window.location.href = "{{url('cp/productsAll/delet_all_products')}}"+'/'+ids;
                }
            });
        })
        
    </script>
@endpush   
@endsection