<div class="modal fade" id="editModal_{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">@lang('site.edit') {{$user->first_name}} {{$user->last_name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        @include('partials._errors')
        <div class="modal-body">
            <form action="{{ route('cp.users.update', $user->id) }}" method="post" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                @csrf
                <div class="form-group">
                    <label>@lang('site.first_name')</label>
                    <input type="text" name="first_name" class="form-control" value="{{ $user->first_name }}">
                </div>

                <div class="form-group">
                    <label>@lang('site.last_name')</label>
                    <input type="text" name="last_name" class="form-control" value="{{ $user->last_name }}">
                </div>

                <div class="form-group">
                    <label>@lang('site.email')</label>
                    <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                </div>

                <div class="form-group">
                    <label>@lang('site.image')</label>
                    <input type="file" name="image" class="form-control image">
                </div>

                <div class="form-group">
                    <img src="{{ $user->image_path }}" style="width: 100px" class="img-thumbnail image-preview" alt="">
                </div>

                <div class="form-group">
                    <label>@lang('site.permissions')</label>
                    <div class="nav-tabs-custom">

                        @php
                            $models = ['users', 'categories', 'products', 'clients', 'orders'];
                            $maps = ['create', 'read', 'update', 'delete'];
                        @endphp

                        <ul class="nav nav-tabs">
                            @foreach ($models as $index=>$model)
                                <li class="{{ $index == 0 ? 'active' : '' }}"><a href="#{{ $model }}_{{$user->id}}" data-toggle="tab">@lang('site.' . $model)</a></li>
                            @endforeach
                        </ul>

                        <div class="tab-content">

                            @foreach ($models as $index=>$model)

                                <div class="tab-pane {{ $index == 0 ? 'active' : '' }}" id="{{ $model }}_{{$user->id}}">

                                    @foreach ($maps as $map)
                                        {{--create_users--}}
                                        <label><input type="checkbox" name="permissions[]" {{ $user->hasPermission($map . '_' . $model) ? 'checked' : '' }} value="{{ $map . '_' . $model }}"> @lang('site.' . $map)</label>
                                    @endforeach

                                </div>

                            @endforeach

                        </div><!-- end of tab content -->

                    </div><!-- end of nav tabs -->

                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> @lang('site.edit')</button>
                </div>

            </form><!-- end of form -->
        </div>
    </div>
    </div>
</div>