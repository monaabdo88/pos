@extends('dashboard.layouts.app')
@section('content')
<section class="content-header">

        <h1>@lang('site.users')</h1>

        <ol class="breadcrumb">
            <li><a href="{{ route('cp.index') }}"><i class="fa fa-dashboard"></i> @lang('site.dashboard')</a></li>
            <li class="active">@lang('site.users')</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                
              <h3 class="box-title">@lang('site.users') : {{$users->count()}}</h3>
              
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                @include('notify::messages')
                @if(auth()->user()->hasPermission('create_users'))
                    <button type="button" class="btn btn-success float-left" data-toggle="modal" data-target="#addModal">
                        <i class="fa fa-plus"></i> @lang('site.add_new_user')
                    </button>
                @endif
                  <br><br>
                  <!-- Add Modal -->
                @include('dashboard.users.create')
                @if($errors->any())
                    <div class="alert alert-danger"><p>{{$errors->first()}}</p></div>
                @endif   
                  <table class="table table-bordered data-table display select" id="users_tbl">
                    <thead>
                        <tr>
                            <th class="no-sort"><input type="checkbox" class="selectAll" value="0"/></th>                                
                            <th>#</th>
                            <th>@lang('site.first_name')</th>
                            <th>@lang('site.last_name')</th>
                            <th>@lang('site.email')</th>
                            <th>@lang('site.image')</th>
                            <th>@lang('site.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $key=>$user)
                        <tr>
                            <td><input type="checkbox" name="all_Items[]" value="{{$user->id}}" class="checkItem"></td>
                            <td>{{$key+1}}</td>
                            <td>{{$user->first_name}}</td>
                            <td>{{$user->last_name}}</td>
                            <td>{{$user->email}}</td>
                            <td><img src="{{$user->image_path}}" class="img-thumbnail img-responsive" width="150" height="150"/></td>
                            <td>  
                                @if(!$user->hasRole('super_admin')|| auth()->user()->id == $user->id)
                                    @if(auth()->user()->hasPermission('update_users'))  
                                        <button class="btn btn-warning" data-toggle="modal" data-target="#editModal_{{$user->id}}"><i class="fa fa-pencil"></i></button>
                                    @endif
                                    @if(auth()->user()->hasPermission('delete_users'))
                                        <a href="{{url('cp/users/delUser/'.$user->id)}}" class="btn btn-danger delete-confirm"><i class="fa fa-trash"></i></a>
                                    @endif
                                @endif
                            </td>
                        </tr>
                            @if(auth()->user()->hasPermission('update_users'))
                                @include('dashboard.users.edit')
                            @endif
                        @endforeach

                    </tbody>
                 
                </table>
                @if(auth()->user()->hasPermission('delete_users'))
                    <button class="btn btn-danger float-right confirm_all" type="submit" disabled>@lang('site.del_all')</button>
                @endif
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
  
           
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
</section>
@push('scripts')
    
    <script>
        $(document).ready(function() {
            $('#users_tbl').DataTable({
                "columnDefs": [ {
                    "targets": 'no-sort',
                    "orderable": false,
                    "order": []
                } ]
            } );
        });
        
       // confirm delete
        $('.delete-confirm').on('click', function (event) {
            event.preventDefault();
            const url = $(this).attr('href');
            swal({
                title: '@lang('site.confirm_delete')',
                text: '@lang('site.delete_msg')',
                icon: 'warning',
                buttons: ["@lang('site.no')", "@lang('site.yes')"],
            }).then(function(value) {
                if (value) {
                    window.location.href = url;
                }
            });
        });
        // enable check all items
        $('.selectAll').click(function () {    
            $(':checkbox.checkItem').prop('checked', this.checked);    
        }); 
        //show delete all button after check checkboxs
        $('.check:button').click(function () {
            var checked = !$(this).data('checked');
            $('input:checkbox').prop('checked', checked);
            $('.confirm_all').prop('disabled', !checked)
            $(this).data('checked', checked);
            if (checked == true) {
                $(this).val('Uncheck All');
            } else if (checked == false) {
                $(this).val('Check All');
            }
        });
        $('input:checkbox').change(function () {
            $('.confirm_all').prop('disabled', $('input:checkbox:checked').length == 0)
        })
        // confirm before delete All
        $('.confirm_all').click(function () {
            event.preventDefault();
            var ids = [];
            swal({
                title: '@lang('site.confirm_delete')',
                text: '@lang('site.delete_msg')',
                icon: 'warning',
                buttons: ["@lang('site.no')", "@lang('site.yes')"],
            }).then(function(value) {
                if (value) {
                    $.each($('.checkItem:checked'),function(){ 
                        if($(this).val() != 0)
                        ids.push($(this).val()); 
                    });
                    window.location.href = "{{url('cp/usersAll/delet_all_users')}}"+'/'+ids;
                }
            });
        })
        
    </script>
@endpush   
@endsection