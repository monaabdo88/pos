<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="addModalLabel">@lang('site.add_new_user')</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
            @include('partials._errors')

            <form action="{{ route('cp.users.store') }}" method="post" enctype="multipart/form-data">

                @csrf
                <div class="form-group">
                    <label>@lang('site.first_name')</label>
                    <input type="text" name="first_name" class="form-control" value="{{ old('first_name') }}">
                </div>

                <div class="form-group">
                    <label>@lang('site.last_name')</label>
                    <input type="text" name="last_name" class="form-control" value="{{ old('last_name') }}">
                </div>

                <div class="form-group">
                    <label>@lang('site.email')</label>
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <label>@lang('site.image')</label>
                    <input type="file" name="image" class="form-control image">
                </div>

                <div class="form-group">
                    <img src="{{ asset('uploads/users/no-image.png') }}"  style="width: 100px" class="img-thumbnail image-preview" alt="">
                </div>

                <div class="form-group">
                    <label>@lang('site.password')</label>
                    <input type="password" name="password" class="form-control">
                </div>

                <div class="form-group">
                    <label>@lang('site.password_confirmation')</label>
                    <input type="password" name="password_confirmation" class="form-control">
                </div>

                <div class="form-group">
                    <label>@lang('site.permissions')</label>
                    <div class="nav-tabs-custom">

                        @php
                            $models = ['users', 'categories', 'products', 'clients', 'orders'];
                            $maps = ['create', 'read', 'update', 'delete'];
                        @endphp

                        <ul class="nav nav-tabs">
                            @foreach ($models as $index=>$model)
                                <li class="{{ $index == 0 ? 'active' : '' }}"><a href="#{{ $model }}" data-toggle="tab">@lang('site.' . $model)</a></li>
                            @endforeach
                        </ul>

                        <div class="tab-content">

                            @foreach ($models as $index=>$model)

                                <div class="tab-pane {{ $index == 0 ? 'active' : '' }}" id="{{ $model }}">

                                    @foreach ($maps as $map)
                                        <label><input type="checkbox" name="permissions[]" value="{{ $map . '_' . $model }}"> @lang('site.' . $map)</label>
                                    @endforeach

                                </div>

                            @endforeach

                        </div><!-- end of tab content -->
                        
                    </div><!-- end of nav tabs -->
                    
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                </div>

            </form><!-- end of form -->
        </div>
        
    </div>
    </div>
</div>