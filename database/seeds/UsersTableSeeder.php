<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::create([
            'first_name' => 'mona',
            'last_name' => 'abdo',
            'email' => 'monaabdo88@gmail.com',
            'password' => bcrypt('12122005'),
        ]);

        $user->attachRole('super_admin');

    }
}
